package com.learnWiremock.service;

import com.github.jenspiegsa.wiremockextension.ConfigureWireMock;
import com.github.jenspiegsa.wiremockextension.InjectServer;
import com.github.jenspiegsa.wiremockextension.WireMockExtension;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.core.Options;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.learnWiremock.constants.MoviesAppConstants;
import com.learnWiremock.dto.Movie;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;


import java.time.LocalDate;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(WireMockExtension.class)
public class MovieRestClientTest {

    MoviesRestClient moviesRestClient;
    WebClient webClient;

    @InjectServer
    WireMockServer wireMockServer;

    @ConfigureWireMock
    Options options =  wireMockConfig()
            .port(8088)
            .notifier(new ConsoleNotifier(true))
            .extensions(new ResponseTemplateTransformer(true));

    @BeforeEach
    void setup(){
        int port = wireMockServer.port();
        String baseUrl = String.format("http://localhost:%s/",port);
        webClient = WebClient.create(baseUrl);
        moviesRestClient = new MoviesRestClient(webClient);
    }

    @Test
    void retriveAllMovies_anyUrl(){

        stubFor(WireMock.get(WireMock.anyUrl())
                .willReturn(WireMock.aResponse()
                .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("allmovies.json")));

        List<Movie> movieList = moviesRestClient.retriveAllMovies();
        System.out.println(movieList);
        assertTrue(movieList.size()>0);
    }

    @Test
    void retriveAllMovies_matchUrl(){

        stubFor(WireMock.get(urlPathEqualTo(MoviesAppConstants.GET_ALL_MOVIES_V1))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("allmovies.json")));

        List<Movie> movieList = moviesRestClient.retriveAllMovies();
        System.out.println(movieList);
        assertTrue(movieList.size()>0);
    }


    @Test
    void retriveMovieById_fixedId(){

        stubFor(WireMock.get(urlPathEqualTo("/movieservice/v1/movie/1"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("movie.json")));

        Integer movieId = 1;
        Movie movie = moviesRestClient.retriveMovieById(movieId);
        assertEquals("Batman Begins", movie.getName());
    }

    @Test
    void retriveMovieById_genericId(){

        stubFor(WireMock.get(urlPathMatching("/movieservice/v1/movie/[0-9]"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("movie-template.json")));

        Integer movieId = 3;
        Movie movie = moviesRestClient.retriveMovieById(movieId);
        assertEquals(3, movie.getMovie_id());
    }

    @Test
    void retriveMovieById_notFound(){

        stubFor(WireMock.get(urlPathMatching("/movieservice/v1/movie/[0-9]+"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.NOT_FOUND.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("404-notFound.json")));

        Integer movieId = 100;
        Assertions.assertThrows(WebClientResponseException.class,()-> moviesRestClient.retriveMovieById(movieId));
    }

    @Test
    void retriveMovieByName_urlPathEqualTo(){

        String movieName = "Avengers";
        stubFor(WireMock.get(urlPathEqualTo(MoviesAppConstants.RETRIVE_MOVIE_BY_NAME))
                .withQueryParam("movie_name", equalTo(movieName))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("retriveMovieByName.json")));

        List<Movie> movieList = moviesRestClient.retriveMovieByName(movieName);
        assertTrue(movieList.size()>0);
    }

    @Test
    void retriveMovieByName_urlEqualTo(){

        String movieName = "Avengers";
        stubFor(WireMock.get(urlEqualTo(MoviesAppConstants.RETRIVE_MOVIE_BY_NAME+"?movie_name="+movieName))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("retriveMovieByName.json")));

        List<Movie> movieList = moviesRestClient.retriveMovieByName(movieName);
        assertTrue(movieList.size()>0);
    }

    @Test
    void retriveMovieByName_responseTemplating(){

        String movieName = "Avengers";
        stubFor(WireMock.get(urlEqualTo(MoviesAppConstants.RETRIVE_MOVIE_BY_NAME+"?movie_name="+movieName))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("retriveMovieByName.json")));

        List<Movie> movieList = moviesRestClient.retriveMovieByName(movieName);
        assertTrue(movieList.size()>0);
    }

  /*  @Test
    void retriveMovieByName_notFound() {
        String movieName = "Nothing";
        Assertions.assertThrows(WebClientResponseException.class, ()-> moviesRestClient.retriveMovieByName(movieName));
    }*/

    @Test
    void retriveMovieByYear_responseTemplating(){

        Integer movieYear = 2012;
        stubFor(WireMock.get(urlEqualTo(MoviesAppConstants.RETRIVE_MOVIE_BY_YEAR+"?year="+movieYear))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("retriveMovieByYear.json")));

        List<Movie> movieList = moviesRestClient.retriveMovieByYear(movieYear);
        assertTrue(movieList.size()>0);
    }

 /*   @Test
    void retriveMovieByYear_notFound() {
        Integer movieYear = 20;
        Assertions.assertThrows(WebClientResponseException.class, ()-> moviesRestClient.retriveMovieByYear(movieYear));
    }
  */
    @Test
    void addMovie(){

        Movie movie = new Movie(null, "S.Bhatt, A.Shibe, S.Singh", "Three Musketeers", 2020, LocalDate.of(2020, 03, 02));

        stubFor(WireMock.post(urlPathEqualTo(MoviesAppConstants.ADD_NEW_MOVIE))
                .withRequestBody(matchingJsonPath(("$.name")))
                .withRequestBody(matchingJsonPath(("$.cast")))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("addMovie.json")));

        Movie addedMovie = moviesRestClient.addMovie(movie);
        assertTrue(addedMovie.getMovie_id()!=null);
    }

    @Test
    void addMovie_responseTemplating(){

        Movie movie = new Movie(null, "S.Bhatt, A.Shibe, S.Singh", "Three Musketeers", 2020, LocalDate.of(2020, 03, 02));

        stubFor(WireMock.post(urlPathEqualTo(MoviesAppConstants.ADD_NEW_MOVIE))
                .withRequestBody(matchingJsonPath(("$.name")))
                .withRequestBody(matchingJsonPath(("$.cast")))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("addMovieTemplate.json")));

        Movie addedMovie = moviesRestClient.addMovie(movie);
        assertTrue(addedMovie.getMovie_id()!=null);
    }

    @Test
    void addMovie_badRequest(){

        Movie movie = new Movie(null, "S.Bhatt, A.Shibe, S.Singh", null, 2020, LocalDate.of(2020, 03, 02));

        stubFor(WireMock.post(urlPathEqualTo(MoviesAppConstants.ADD_NEW_MOVIE))
                .withRequestBody(matchingJsonPath(("$.cast")))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.BAD_REQUEST.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("404-invalidInput.json")));

        String expectedErrorMessage = "Please pass all the input fields : [name]";
        Assertions.assertThrows(WebClientResponseException.class, ()-> moviesRestClient.addMovie(movie) , expectedErrorMessage);

    }


    @Test
    void updateMovie(){

        Integer movieId = 1;
        Integer year = 2020;
        Movie movie = new Movie(null, null, null, year, null);

        stubFor(WireMock.put(urlPathMatching("/movieservice/v1/movie/[0-9]+"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("updateMovieTemplate.json")));

        Movie updatedMovie = moviesRestClient.updateMovie(movieId, movie);
        assertTrue(updatedMovie.getYear() == 2020);
    }

    @Test
    void updateMovie_badRequest(){

        Integer movieId = 100;
        Integer year = 2020;
        Movie movie = new Movie(null, null, null, year, null);

        stubFor(WireMock.put(urlPathMatching("/movieservice/v1/movie/[0-9]+"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.NOT_FOUND.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)));

        Assertions.assertThrows(WebClientResponseException.class, ()-> moviesRestClient.updateMovie(movieId,movie));

    }

    @Test
    void deleteMovie() {

        Movie movie = new Movie(null,"Tom Hanks, Tim Allen","Toys Story 5", 2019, LocalDate.of(2019, 06, 20));

        stubFor(post(urlPathEqualTo(MoviesAppConstants.ADD_NEW_MOVIE))
                .withRequestBody(matchingJsonPath(("$.name"),equalTo("Toys Story 5")))
                .withRequestBody(matchingJsonPath(("$.cast"), containing("Tom")))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBodyFile("addMovieTemplate.json")));
        Movie addedMovie = moviesRestClient.addMovie(movie);

        String expectedErrorMessage = "Movie Deleted Successfully";

        stubFor(delete(urlPathMatching("/movieservice/v1/movie/[0-9]+"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBody(expectedErrorMessage)));

        String responseMessage = moviesRestClient.deleteMovie(addedMovie.getMovie_id().intValue());

        assertEquals(expectedErrorMessage, responseMessage);
    }

    @Test
    void deleteMovie_NotFound() {
        //given
        Integer id = 100;
        stubFor(delete(urlPathMatching("/movieservice/v1/movie/[0-9]+"))
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.NOT_FOUND.value())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)));

        //then
        Assertions.assertThrows(WebClientResponseException.class, () -> moviesRestClient.deleteMovie(id));

    }

}
