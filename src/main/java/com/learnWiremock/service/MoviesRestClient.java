package com.learnWiremock.service;

import com.learnWiremock.constants.MoviesAppConstants;
import com.learnWiremock.dto.Movie;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;


@Slf4j
public class MoviesRestClient {

    private WebClient webClient;
    public MoviesRestClient(WebClient webClient){
        this.webClient = webClient;
    }

    public List<Movie> retriveAllMovies(){

       return webClient.get().uri(MoviesAppConstants.GET_ALL_MOVIES_V1)
                .retrieve()
                .bodyToFlux(Movie.class)
                .collectList()
                .block();

    }

    public Movie retriveMovieById(Integer movieId){

        try {
            return webClient.get().uri(MoviesAppConstants.RETRIVE_MOVIE_BY_ID, movieId)
                    .retrieve()
                    .bodyToMono(Movie.class)
                    .block();
        }

        catch (WebClientResponseException ex){
            log.error("WebClientException in retriveMovieById :-{} Status Code :- {} ", ex.getResponseBodyAsString(), ex.getStatusCode());
            throw ex;
        }

        catch (Exception ex){
            log.error("Exception in retriveMovieById:- {}" ,ex);
            throw ex;
        }
    }

    public List<Movie> retriveMovieByName(String movieName){

        try {
            String retriveByName = UriComponentsBuilder.fromUriString(MoviesAppConstants.RETRIVE_MOVIE_BY_NAME)
                    .queryParam("movie_name", movieName)
                    .buildAndExpand()
                    .toUriString();

            return webClient.get().uri(retriveByName)
                    .retrieve()
                    .bodyToFlux(Movie.class)
                    .collectList()
                    .block();
        }

        catch (WebClientResponseException ex){
            log.error("WebClientException in retriveMovieByName :-{} Status Code :- {} ", ex.getResponseBodyAsString(), ex.getStatusCode());
            throw ex;
        }

        catch (Exception ex){
            log.error("Exception in retriveMovieByName:- {}" ,ex);
            throw ex;
        }
    }

    public List<Movie> retriveMovieByYear(Integer movieYear){

        try {
            String retriveByYear = UriComponentsBuilder.fromUriString(MoviesAppConstants.RETRIVE_MOVIE_BY_YEAR)
                    .queryParam("year", movieYear)
                    .buildAndExpand()
                    .toUriString();

            return webClient.get().uri(retriveByYear)
                    .retrieve()
                    .bodyToFlux(Movie.class)
                    .collectList()
                    .block();
        }

        catch (WebClientResponseException ex){
            log.error("WebClientException in retriveMovieByYear :-{} Status Code :- {} ", ex.getResponseBodyAsString(), ex.getStatusCode());
            throw ex;
        }

        catch (Exception ex){
            log.error("Exception in retriveMovieByYear:- {}" ,ex);
            throw ex;
        }
    }

    public Movie addMovie(Movie newMovie){

        try {
            return webClient.post().uri(MoviesAppConstants.ADD_NEW_MOVIE)
                    .syncBody(newMovie)
                    .retrieve()
                    .bodyToMono(Movie.class)
                    .block();
        }

        catch (WebClientResponseException ex){
            log.error("WebClientException in addMovie :-{} Status Code :- {} ", ex.getResponseBodyAsString(), ex.getStatusCode());
            throw ex;
        }
        catch (Exception ex){
            log.error("Exception in addMovie:- {}" ,ex);
            throw ex;
        }
    }

    public Movie updateMovie(Integer movieId, Movie movie){

        try {
            return webClient.put().uri(MoviesAppConstants.RETRIVE_MOVIE_BY_ID , movieId)
                    .syncBody(movie)
                    .retrieve()
                    .bodyToMono(Movie.class)
                    .block();
        }

        catch (WebClientResponseException ex){
            log.error("WebClientException in updateMovie :-{} Status Code :- {} ", ex.getResponseBodyAsString(), ex.getStatusCode());
            throw ex;
        }
        catch (Exception ex){
            log.error("Exception in updateMovie:- {}" ,ex);
            throw ex;
        }
    }

    public String deleteMovie(Integer movieId){

        try {
            return webClient.delete().uri(MoviesAppConstants.RETRIVE_MOVIE_BY_ID , movieId)
                    .retrieve()
                    .bodyToMono(String.class)
                    .block();
        }

        catch (WebClientResponseException ex){
            log.error("WebClientException in deleteMovie :-{} Status Code :- {} ", ex.getResponseBodyAsString(), ex.getStatusCode());
            throw ex;
        }
        catch (Exception ex){
            log.error("Exception in deleteMovie:- {}" ,ex);
            throw ex;
        }
    }
}
